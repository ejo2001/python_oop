from datetime import datetime
from functools import wraps

def Log_Decorator(log_f):
    def Log_Dec(function):
        @wraps(function)
        def Log(*args, **kwargs):
            print("Logging")
            with open(f"{log_f}", "a") as log_file:
                log_file.write("Running function " + str(function) + " (" + str(datetime.now()) + ")")
                result = function(*args, **kwargs)
                log_file.write("\n")
                log_file.write("Ran function " + str(function) + " (" + str(datetime.now()) + ")")
                log_file.write("\n")
            return result
        return Log

    return Log_Dec
