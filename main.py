import logger

@logger.Log_Decorator("Log.txt")
def add(arg1, arg2):
    return(int(arg1) + int(arg2))
    
if __name__ == "__main__":
    print(add(5, 7))
